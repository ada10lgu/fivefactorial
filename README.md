www.fivefactorial.se

=========================
==                     ==
==   Network handler   ==
==                     ==
=========================
=
= * Added 2017-02-09
= * By Mezz
=
=

Support for unencrypted network transmissions following the following form for a "packet"

|--------------------------------------------------|
| TYPE | LOAD_SIZE | LOAD | PACKETS_SIZE | PACKETS |
|  1b  |     4b    |  *   |      4b      |    *    |
|--------------------------------------------------|

TYPE		= unique identifier for the type of packet sent/recieved
LOAD_SIZE 	= the langth of the LOAD field in bytes (zero length is supported, then the next field is omitted)
LOAD		= Data in byte form
PACKET_SIZE	= the length of the PACKETS field in bytes, follows same rules as LOAD_SIZE
PACKETS		= All extra packages sent with this packet.



