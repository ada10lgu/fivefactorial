package se.fivefactorial.network.verbose;

import se.fivefactorial.network.connection.packet.Packet;

public abstract class Verbose {
	protected boolean verbose = false;
	protected boolean debug = false;

	public boolean isVerbose() {
		return verbose;
	}

	public boolean isDebug() {
		return debug;
	}

	public void setVerbose(boolean verbose) {
		this.verbose = verbose;
	}

	public void setDebug(boolean debug) {
		this.debug = debug;
	}

	public final void print(String s) {
		if (verbose)
			verbosePrint(s);
	}

	public final void printPacket(Packet p) {
		if (verbose)
			verbosePacket(p);
	}

	public final void debug(String s) {
		if (debug)
			verboseDebug(s);
	}

	protected abstract void verbosePrint(String s);

	protected abstract void verbosePacket(Packet p);

	protected abstract void verboseDebug(String s);
}