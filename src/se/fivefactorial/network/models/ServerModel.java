package se.fivefactorial.network.models;

import java.util.Map;

public interface ServerModel {

	void verifyHandshake(Map<String, Object> data);

}
