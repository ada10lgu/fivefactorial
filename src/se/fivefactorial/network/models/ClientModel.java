package se.fivefactorial.network.models;

import java.util.List;
import java.util.Map;

public interface ClientModel {

	public Map<String, Object> handshake(List<String> headers);

}
