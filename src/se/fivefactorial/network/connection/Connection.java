package se.fivefactorial.network.connection;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.LinkedList;

import se.fivefactorial.network.connection.factory.ConnectionFactory;
import se.fivefactorial.network.connection.factory.MainPacketFactory;
import se.fivefactorial.network.connection.factory.PacketFactory;
import se.fivefactorial.network.connection.packet.SuperPacket;
import se.fivefactorial.network.connection.packet.operands.OperatorPacket;
import se.fivefactorial.network.connection.packet.operands.SetConnectedPacket;
import se.fivefactorial.network.connection.packet.operands.handshake.HandshakeRequestPacket;
import se.fivefactorial.network.verbose.Verbose;

public class Connection {

	private Socket socket;
	private Verbose verbose;
	private MainPacketFactory factory;

	private LinkedList<SuperPacket> outbox;
	private LinkedList<SuperPacket> inbox;
	private TokenList tokenList;
	private HashMap<Token, OperatorPacket> responses;

	private ConnectionState state;

	private ByteSender bs;
	private ByteReciever br;
	private PacketHandler ph;

	public Connection(Socket s, Verbose verbose) {
		setup(s, verbose);
	}

	public Connection(String addr, int port, Verbose verbose) throws IOException {
		Socket s = new Socket();
		s.connect(new InetSocketAddress(addr, port), 5000);
		setup(s, verbose);
	}

	private void setup(Socket socket, Verbose verbose) {
		this.socket = socket;
		this.verbose = verbose;

		state = ConnectionState.SETUP;

		outbox = new LinkedList<>();
		inbox = new LinkedList<>();
		tokenList = new TokenList();
		responses = new HashMap<>();

		factory = new MainPacketFactory();
		
		factory.addFactory(new ConnectionFactory(this));
	}

	public synchronized void addFactory(PacketFactory f) {
		if (state == ConnectionState.SETUP) {
			factory.addFactory(f);
		} else {
			throw new IllegalArgumentException("Cannot add factories outside of setup mode");
		}
	}

	public synchronized void enableHandshake() {
		(bs = new ByteSender()).start();
		(br = new ByteReciever()).start();
		(ph = new PacketHandler()).start();
		state = ConnectionState.HANDSHAKING;
	}

	public synchronized void handshake(HandshakeRequestPacket packet) throws InterruptedException {
		(bs = new ByteSender()).start();
		(br = new ByteReciever()).start();
		(ph = new PacketHandler()).start();
		state = ConnectionState.HANDSHAKING;

		if (packet == null)
			throw new NullPointerException("(Connection.java) Can't send null");
		Token token = tokenList.get();
		SuperPacket sp = new SuperPacket(packet, token);
		outbox.offer(sp);
		notifyAll();

		OperatorPacket op = recieve(token);
		op.perform();
		state = ConnectionState.CONNECTED;
		sendBlind(new SetConnectedPacket());
	}

	public synchronized ConnectionState getState() {
		return state;
	}

	public synchronized void setConnected() {
		if (state == ConnectionState.HANDSHAKING)
			state = ConnectionState.CONNECTED;
		notifyAll();
	}
	

	private synchronized void waitForState(ConnectionState state) throws InterruptedException {
		while (this.state != state)
			wait();
	}

	public synchronized void waitForStaitChange() throws InterruptedException {
		ConnectionState old = state;
		while (state == old)
			wait();
	}

	public synchronized Token send(OperatorPacket op) throws InterruptedException {
		if (op == null)
			throw new NullPointerException("(Connection.java) Can't send null");
		waitForState(ConnectionState.CONNECTED);
		Token token = tokenList.get();
		SuperPacket sp = new SuperPacket(op, token);
		outbox.offer(sp);
		notifyAll();
		return token;
	}

	public synchronized void sendBlind(OperatorPacket op) throws InterruptedException {
		new ResponseWaiter(op);
	}

	public synchronized OperatorPacket recieve(Token token) throws InterruptedException {
		OperatorPacket op = null;
		while ((op = responses.remove(token)) == null)
			wait();
		tokenList.give(token);
		return op;
	}

	private synchronized void addOutgoing(SuperPacket sp) {
		outbox.offer(sp);
		notifyAll();
	}

	private synchronized SuperPacket getOutgoing() throws InterruptedException {
		while (outbox.isEmpty() && state != ConnectionState.OFFLINE)
			wait();
		return outbox.poll();
	}

	private synchronized void addIncomming(SuperPacket sp) {
		inbox.offer(sp);
		notifyAll();
	}

	private synchronized SuperPacket getIncomming() throws InterruptedException {
		while (inbox.isEmpty() && state != ConnectionState.OFFLINE)
			wait();
		return inbox.poll();
	}

	private synchronized void addResponse(Token token, OperatorPacket op) {
		responses.put(token, op);
		notifyAll();
	}

	private class ByteSender extends Thread {

		@Override
		public void run() {
			verbose.debug("Byte sender started");
			try {
				OutputStream os = socket.getOutputStream();
				while (state != ConnectionState.OFFLINE) {
					SuperPacket sp = getOutgoing();
					if (sp != null) {
						verbose.printPacket(sp);
						byte[] data = sp.getData();

						os.write(intToByte(data.length));
						os.write(data);
						os.flush();
					}
				}
			} catch (IOException e) {
				close();
			} catch (InterruptedException e) {
				close();
			}
			verbose.debug("Byte sender offline");
		}

		private byte[] intToByte(int i) {
			return ByteBuffer.allocate(4).putInt(i).array();
		}
	}

	private class ByteReciever extends Thread {

		@Override
		public void run() {
			verbose.debug("Byte reciever started");
			try {
				InputStream is = socket.getInputStream();

				while (state != ConnectionState.OFFLINE) {
					byte[] l = new byte[4];
					for (int i = 0; i < 4; i++) {
						l[i] = readByte(is);
					}
					int length = byteToInt(l);
					byte[] data = new byte[length];
					for (int i = 0; i < length; i++) {
						data[i] = readByte(is);
					}
					SuperPacket sp = (SuperPacket) factory.create(data);
					addIncomming(sp);
				}
			} catch (IOException e) {
				close();
			}
			verbose.debug("Byte reciever offline");
		}

		private byte readByte(InputStream is) throws IOException {
			int input = is.read();
			if (input == -1)
				throw new IOException("EOF");
			return (byte) input;
		}

		private int byteToInt(byte[] b) {
			ByteBuffer bb = ByteBuffer.wrap(b);
			return bb.getInt();
		}
	}

	private class PacketHandler extends Thread {

		@Override
		public void run() {
			verbose.debug("Packet handler started");
			try {
				while (state != ConnectionState.OFFLINE) {
					SuperPacket sp = getIncomming();
					if (sp != null) {
						verbose.printPacket(sp);
						if (sp.isAck()) {
							OperatorPacket op = sp.getPacket();
							Token token = sp.getToken();
							addResponse(token, op);
						} else {
							SuperPacket ack = sp.getAck();
							addOutgoing(ack);
						}
					}
				}
			} catch (InterruptedException e) {
				close();
			}
			verbose.debug("Packet handler offline");
		}
	}

	private class ResponseWaiter extends Thread {
		private OperatorPacket op;

		public ResponseWaiter(OperatorPacket op) {
			this.op = op;
			start();
		}

		@Override
		public void run() {
			try {
				Token t = send(op);
				recieve(t);
			} catch (InterruptedException e) {
			}
		}
	}

	public synchronized void close() {
		try {
			state = ConnectionState.OFFLINE;
			socket.close();
			br.interrupt();
			bs.interrupt();
			ph.interrupt();
			notifyAll();
		} catch (IOException e) {
		}
	}



}