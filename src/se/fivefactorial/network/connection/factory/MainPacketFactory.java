package se.fivefactorial.network.connection.factory;

import java.util.ArrayList;
import java.util.List;

import se.fivefactorial.network.connection.ByteArrayReader;
import se.fivefactorial.network.connection.packet.Packet;
import se.fivefactorial.network.connection.packet.SuperPacket;
import se.fivefactorial.network.connection.packet.data.ArrayPacket;
import se.fivefactorial.network.connection.packet.data.BytePacket;
import se.fivefactorial.network.connection.packet.data.FilePacket;
import se.fivefactorial.network.connection.packet.data.IntPacket;
import se.fivefactorial.network.connection.packet.data.JSONPacket;
import se.fivefactorial.network.connection.packet.data.StringPacket;
import se.fivefactorial.network.connection.packet.data.TokenPacket;
import se.fivefactorial.network.connection.packet.operands.NullPacket;
import se.fivefactorial.network.connection.packet.operands.ResponsePacket;

public class MainPacketFactory implements PacketFactory {

	private List<PacketFactory> factories;

	public MainPacketFactory() {
		factories = new ArrayList<>();
	}

	public void addFactory(PacketFactory f) {
		factories.add(f);
	}

	@Override
	public Packet create(byte type, byte[] data, Packet[] packets) {

		Packet p = null;

		switch (type) {
		case Packet.SUPER_PACKET:
			p = new SuperPacket(data, packets);
			break;
		case Packet.TOKEN_PACKET:
			p = new TokenPacket(data, packets);
			break;
		case Packet.NULL_PACKET:
			p = new NullPacket();
			break;
		case Packet.INT_PACKET:
			p = new IntPacket(data, packets);
			break;
		case Packet.BYTE_PACKET:
			p = new BytePacket(data, packets);
			break;
		case Packet.JSON_PACKET:
			p = new JSONPacket(data, packets);
			break;
		case Packet.FILE_PACKE:
			p = new FilePacket(data, packets);
			break;
		case Packet.STRING_PACKET:
			p = new StringPacket(data, packets);
			break;
		case Packet.ARRAY_PACKET:
			p = new ArrayPacket(data, packets);
			break;
		case Packet.RESPONSE_PACKET:
			p = new ResponsePacket(data, packets);
		}

		for (int i = 0; i < factories.size() && p == null; i++) {
			p = factories.get(i).create(type, data, packets);
		}

		return p;
	}

	public Packet create(byte[] data) {
		ByteArrayReader bar = new ByteArrayReader(data);
		return create(bar);
	}

	private Packet create(ByteArrayReader bar) {
		byte type = bar.next();
		int dataSize = bar.nextInt();
		byte[] data = new byte[dataSize];
		bar.fillArray(data);

		int packetSize = bar.nextInt();
		Packet[] packets = new Packet[packetSize];
		for (int i = 0; i < packetSize; i++) {
			packets[i] = create(bar);
		}
		Packet p = create(type, data, packets);
		if (p == null)
			throw new IllegalArgumentException("Unparsable type (" + type + ")" + bar.toString());
		return p;
	}

}
