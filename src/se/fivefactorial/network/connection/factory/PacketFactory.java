package se.fivefactorial.network.connection.factory;

import se.fivefactorial.network.connection.packet.Packet;

public interface PacketFactory {

	public Packet create(byte type, byte[] data, Packet[] packets);
}
