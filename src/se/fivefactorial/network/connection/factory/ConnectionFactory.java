package se.fivefactorial.network.connection.factory;

import se.fivefactorial.network.connection.Connection;
import se.fivefactorial.network.connection.packet.Packet;
import se.fivefactorial.network.connection.packet.operands.SetConnectedPacket;

public class ConnectionFactory implements PacketFactory {

	private Connection conn;

	public ConnectionFactory(Connection conn) {
		this.conn = conn;
	}

	@Override
	public Packet create(byte type, byte[] data, Packet[] packets) {
		Packet p = null;
		switch (type) {
		case Packet.SET_CONNECTED_PACKET:
			p = new SetConnectedPacket(data, packets, conn);
			break;
		}
		return p;
	}

}
