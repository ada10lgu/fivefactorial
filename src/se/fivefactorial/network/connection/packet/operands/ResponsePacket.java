package se.fivefactorial.network.connection.packet.operands;

import se.fivefactorial.network.connection.packet.Packet;

public class ResponsePacket extends OperatorPacket {

	private Packet p;

	public ResponsePacket(Packet p) {
		this.p = p;
	}

	public ResponsePacket(byte[] data, Packet[] packets) {
		p = packets[0];
	}

	public Packet getPacket() {
		return p;
	}

	@Override
	protected Packet[] getPackages() {
		return new Packet[] { p };
	}

	@Override
	public OperatorPacket perform() {
		return new NullPacket();
	}

	@Override
	protected byte getType() {
		return Packet.RESPONSE_PACKET;
	}
	
	@Override
	public String toString() {
		return "Response [" + p + "]";
	}
}