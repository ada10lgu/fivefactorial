package se.fivefactorial.network.connection.packet.operands;

import se.fivefactorial.network.connection.packet.Packet;

public abstract class OperatorPacket extends Packet {

	public abstract OperatorPacket perform();

}
