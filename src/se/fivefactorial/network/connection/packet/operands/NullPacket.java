package se.fivefactorial.network.connection.packet.operands;

import se.fivefactorial.network.connection.packet.Packet;

public class NullPacket extends OperatorPacket {

	@Override
	public OperatorPacket perform() {
		return new NullPacket();
	}

	@Override
	protected byte getType() {
		return Packet.NULL_PACKET;
	}

	@Override
	public boolean isNull() {
		return true;
	}

	@Override
	public String toString() {
		return "null";
	}

}
