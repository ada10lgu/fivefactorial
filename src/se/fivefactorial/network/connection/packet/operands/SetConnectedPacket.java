package se.fivefactorial.network.connection.packet.operands;

import se.fivefactorial.network.connection.Connection;
import se.fivefactorial.network.connection.packet.Packet;

public class SetConnectedPacket extends OperatorPacket {
	private Connection conn;

	public SetConnectedPacket() {

	}

	public SetConnectedPacket(byte[] data, Packet[] packets, Connection conn) {
		this.conn = conn;
	}

	@Override
	public OperatorPacket perform() {
		conn.setConnected();
		return new NullPacket();
	}

	@Override
	protected byte getType() {
		return Packet.SET_CONNECTED_PACKET;
	}
	
	@Override
	public String toString() {
		return "Set connected";
	}

}
