package se.fivefactorial.network.connection.packet.operands.handshake;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import se.fivefactorial.network.connection.packet.Packet;
import se.fivefactorial.network.connection.packet.data.JSONPacket;
import se.fivefactorial.network.connection.packet.operands.NullPacket;
import se.fivefactorial.network.connection.packet.operands.OperatorPacket;
import se.fivefactorial.network.models.ServerModel;

public class HandshakeResponsePacket extends OperatorPacket {

	private ServerModel model;
	private JSONPacket json;

	HandshakeResponsePacket(Map<String, Object> data) {
		JSONObject obj = new JSONObject();
		for (String s : data.keySet()) {
			obj.put(s, data.get(s));
		}
		json = new JSONPacket(obj);
	}

	public HandshakeResponsePacket(byte[] data, Packet[] packets, ServerModel model) {
		this.model = model;
		json = (JSONPacket) packets[0];
	}

	@Override
	protected Packet[] getPackages() {
		return new Packet[] { json };
	}

	@Override
	public OperatorPacket perform() {
		Map<String, Object> data = new HashMap<>();
		for (String s : json.getJSON().keySet()) {
			data.put(s, json.getJSON().get(s));
		}
		model.verifyHandshake(data);
		return new NullPacket();
	}

	@Override
	protected byte getType() {
		return Packet.HANDSHAKE_RESPONSE_PACKET;
	}

	@Override
	public String toString() {
		return "Handshake response " + json.getJSON();
	}

}
