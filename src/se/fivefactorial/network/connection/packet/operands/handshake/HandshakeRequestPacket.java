package se.fivefactorial.network.connection.packet.operands.handshake;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import se.fivefactorial.network.connection.packet.Packet;
import se.fivefactorial.network.connection.packet.data.JSONPacket;
import se.fivefactorial.network.connection.packet.operands.OperatorPacket;
import se.fivefactorial.network.models.ClientModel;

public class HandshakeRequestPacket extends OperatorPacket {

	private static final String HEADERS = "headers";

	private ClientModel model;
	private JSONPacket json;

	public HandshakeRequestPacket(String[] headers) {
		JSONArray array = new JSONArray();
		for (String s : headers)
			array.put(s);
		JSONObject obj = new JSONObject();
		obj.put(HEADERS, array);
		json = new JSONPacket(obj);
	}

	public HandshakeRequestPacket(byte[] data, Packet[] packets, ClientModel model) {
		this.model = model;
		json = (JSONPacket) packets[0];
	}

	@Override
	public OperatorPacket perform() {
		List<String> headers = new ArrayList<>();
		JSONArray array = json.getJSON().getJSONArray(HEADERS);
		for (Object s : array) {
			headers.add(s.toString());
		}
		Map<String, Object> result = model.handshake(headers);
		return new HandshakeResponsePacket(result);
	}

	@Override
	protected Packet[] getPackages() {
		return new Packet[] { json };
	}

	@Override
	protected byte getType() {
		return Packet.HANDSHAKE_REQUEST_PACKET;
	}
	
	@Override
	public String toString() {
		return "Handshake request "+ json.getJSON().getJSONArray(HEADERS);
	}
}