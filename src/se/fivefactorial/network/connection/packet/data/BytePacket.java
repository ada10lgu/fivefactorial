package se.fivefactorial.network.connection.packet.data;

import se.fivefactorial.network.connection.packet.Packet;

public class BytePacket extends Packet {

	private byte b;

	public BytePacket(byte b) {
		this.b = b;
	}

	public BytePacket(byte[] data, Packet[] packets) {
		if (data.length != 1)
			throw new IllegalArgumentException("Illgal lenght on data");
		if (packets.length != 0)
			throw new IllegalArgumentException("Does not support packets");
		b = data[0];
	}

	@Override
	protected byte[] getLoad() {
		return new byte[] { b };
	}

	@Override
	protected byte getType() {
		return Packet.BYTE_PACKET;
	}

	public byte getByte() {
		return b;
	}

	@Override
	public String toString() {
		return b + "b";
	}
}
