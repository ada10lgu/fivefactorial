package se.fivefactorial.network.connection.packet.data;

import org.json.JSONObject;

import se.fivefactorial.network.connection.packet.Packet;

public class JSONPacket extends Packet {

	public JSONObject obj;

	public JSONPacket(JSONObject obj) {
		this.obj = obj;
	}

	public JSONPacket(byte[] data, Packet[] packets) {
		String s = new String(data);
		s.trim();
		obj = new JSONObject(s);
	}

	@Override
	protected byte[] getLoad() {
		return obj.toString().getBytes();
	}

	@Override
	protected byte getType() {
		return Packet.JSON_PACKET;
	}

	public JSONObject getJSON() {
		return obj;
	}

}
