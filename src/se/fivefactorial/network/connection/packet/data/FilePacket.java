package se.fivefactorial.network.connection.packet.data;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;

import se.fivefactorial.network.connection.packet.Packet;

public class FilePacket extends Packet {

	private byte[] data;

	public FilePacket(File f) throws IOException {
		data = Files.readAllBytes(f.toPath());
	}

	public FilePacket(byte[] data, Packet[] packets) {
		this.data = data;
	}

	@Override
	protected byte[] getLoad() {
		return data;
	}

	@Override
	protected byte getType() {
		return Packet.FILE_PACKE;
	}

	public void save(File f) throws IOException {
		FileOutputStream fos = new FileOutputStream(f);
		fos.write(data);
		fos.close();
	}

	public byte[] fileToByte() {
		return getLoad();
	}

	@Override
	public String toString() {
		return "File (" + fileToByte().length + "b)";
	}

}
