package se.fivefactorial.network.connection.packet.data;

import se.fivefactorial.network.connection.Token;
import se.fivefactorial.network.connection.packet.Packet;

public class TokenPacket extends Packet {

	private Token t;

	public TokenPacket(Token t) {
		this.t = t;
	}

	public TokenPacket(byte[] data, Packet[] packets) {
		t = new Token(data);
	}

	@Override
	protected byte[] getLoad() {
		return t.toByte();
	}

	@Override
	protected byte getType() {
		return TOKEN_PACKET;
	}

	public Token getToken() {
		return t;
	}
	
	@Override
	public String toString() {
		return t.toString();
	}

}
