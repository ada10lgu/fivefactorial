package se.fivefactorial.network.connection.packet.data;

import java.util.ArrayList;
import java.util.List;

import se.fivefactorial.network.connection.packet.Packet;

public class ArrayPacket extends Packet {

	private List<Packet> packets;

	public ArrayPacket(Packet... packets) {
		this.packets = new ArrayList<>();
		for (Packet p : packets) {
			this.packets.add(p);
		}
	}

	public ArrayPacket(List<? extends Packet> packets) {
		this.packets = new ArrayList<>();
		for (Packet p : packets) {
			this.packets.add(p);
		}
	}

	public ArrayPacket(byte[] data, Packet[] packets) {
		this.packets = new ArrayList<>();
		for (Packet p : packets) {
			this.packets.add(p);
		}
	}

	public int size() {
		return packets.size();
	}

	@SuppressWarnings("unchecked")
	public <E extends Packet> E get(int i) {
		return (E) packets.get(i);
	}

	@Override
	protected Packet[] getPackages() {
		Packet[] list = new Packet[size()];
		packets.toArray(list);
		return list;
	}

	@Override
	protected byte getType() {
		return Packet.ARRAY_PACKET;
	}

	@Override
	public String toString() {
		return packets.toString();
	}

}
