package se.fivefactorial.network.connection.packet.data;

import java.nio.ByteBuffer;

import se.fivefactorial.network.connection.packet.Packet;

public class IntPacket extends Packet {

	private int i;

	public IntPacket(int i) {
		this.i = i;
	}

	public IntPacket(byte[] data, Packet[] packets) {
		ByteBuffer bb = ByteBuffer.wrap(data);
		i = bb.getInt();
		if (packets.length != 0)
			throw new IllegalArgumentException("Does not support packets");
	}

	@Override
	protected byte[] getLoad() {
		return ByteBuffer.allocate(4).putInt(i).array();
	}

	@Override
	protected byte getType() {
		return Packet.INT_PACKET;
	}

	public int getInt() {
		return i;
	}
	
	@Override
	public String toString() {
		return "" + i;
	}

}
