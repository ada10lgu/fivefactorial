package se.fivefactorial.network.connection.packet.data;

import se.fivefactorial.network.connection.packet.Packet;

public class StringPacket extends Packet {

	private String s;

	public StringPacket(String s) {
		this.s = s;
	}

	public StringPacket(byte[] data, Packet[] packets) {
		s = new String(data);
	}

	@Override
	protected byte[] getLoad() {
		return s.getBytes();
	}

	@Override
	protected byte getType() {
		return Packet.STRING_PACKET;
	}

	public String getString() {
		return s;
	}
	
	@Override
	public String toString() {
		return "\"" + s + "\"";
	}

}
