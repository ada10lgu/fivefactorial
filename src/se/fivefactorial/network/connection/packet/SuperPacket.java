package se.fivefactorial.network.connection.packet;

import se.fivefactorial.network.connection.Token;
import se.fivefactorial.network.connection.packet.data.BytePacket;
import se.fivefactorial.network.connection.packet.data.TokenPacket;
import se.fivefactorial.network.connection.packet.operands.OperatorPacket;

public class SuperPacket extends Packet {

	private final static byte ACK = 0;
	private final static byte SEND = -1;

	private OperatorPacket load;
	private BytePacket direction;
	private TokenPacket token;

	public SuperPacket(OperatorPacket p, Token t) {
		this(p, new TokenPacket(t), SEND);
	}

	private SuperPacket(OperatorPacket p, TokenPacket token, byte direction) {
		load = p;
		this.token = token;
		this.direction = new BytePacket(direction);
	}

	public SuperPacket(byte[] data, Packet[] packets) {
		direction = (BytePacket) packets[0];
		token = (TokenPacket) packets[1];
		load = (OperatorPacket) packets[2];
	}

	public SuperPacket getAck() {
		OperatorPacket response = load.perform();
		SuperPacket sp = new SuperPacket(response, token, ACK);
		return sp;
	}

	public boolean isAck() {
		return direction.getByte() == ACK;
	}

	public OperatorPacket getPacket() {
		return load;
	}

	public Token getToken() {
		return token.getToken();
	}

	protected byte[] getmLoad() {
		return new byte[0];
	}

	@Override
	protected Packet[] getPackages() {
		return new Packet[] { direction, token, load };
	}

	@Override
	protected byte getType() {
		return SUPER_PACKET;
	}

	@Override
	public String toString() {
		String w = direction.getByte() == SEND ? "SEND" : "ACK";
		String t = token.toString();
		return "Super! (" + w + " id" + t + "): " + load;
	}
}