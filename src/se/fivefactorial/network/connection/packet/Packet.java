package se.fivefactorial.network.connection.packet;

import java.nio.ByteBuffer;
import java.util.Arrays;

public abstract class Packet {

	// Super packet
	public static final byte SUPER_PACKET = 1;

	// Data packets
	public static final byte TOKEN_PACKET = 2;
	public static final byte BYTE_PACKET = 3;
	public static final byte NULL_PACKET = 4;
	public static final byte INT_PACKET = 5;
	public static final byte JSON_PACKET = 6;
	public static final byte FILE_PACKE = 7;
	public static final byte STRING_PACKET = 8;
	public static final byte ARRAY_PACKET = 9;

	// Operator packets
	public static final byte HANDSHAKE_REQUEST_PACKET = 10;
	public static final byte HANDSHAKE_RESPONSE_PACKET = 11;
	public static final byte RESPONSE_PACKET = 12;
	public static final byte SET_CONNECTED_PACKET = 13;

	public static final byte LAST_BOUND_ID = 13;

	protected byte[] getLoad() {
		return new byte[0];
	}

	protected Packet[] getPackages() {
		return new Packet[0];
	}

	protected abstract byte getType();

	public boolean isNull() {
		return false;
	}

	public final byte[] getData() {
		int size = 9;
		byte[] load = getLoad();
		size += load.length;
		Packet[] packets = getPackages();
		for (Packet p : packets)
			size += p.getData().length;

		byte[] data = new byte[size];
		int i = 0;
		data[i++] = getType();
		for (byte b : intToByte(load.length)) {
			data[i++] = b;
		}
		for (byte b : load)
			data[i++] = b;
		for (byte b : intToByte(packets.length)) {
			data[i++] = b;
		}
		for (Packet p : packets) {
			byte[] temp = p.getData();
			for (byte b : temp)
				data[i++] = b;
		}

		return data;
	}

	private byte[] intToByte(int i) {
		return ByteBuffer.allocate(4).putInt(i).array();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Packet) {
			Packet p = (Packet) obj;
			byte[] a = getData();
			byte[] b = p.getData();
			return Arrays.equals(a, b) && getType() == p.getType();
		}
		return false;
	}
}