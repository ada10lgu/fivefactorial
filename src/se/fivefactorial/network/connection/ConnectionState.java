package se.fivefactorial.network.connection;

public enum ConnectionState {

	SETUP, HANDSHAKING, CONNECTED, OFFLINE;

}
