package se.fivefactorial.network.connection;

public class ConnectionStateWatcher extends Thread {

	private Connection c;

	public ConnectionStateWatcher(Connection c) {
		this.c = c;
		start();
	}

	@Override
	public void run() {
		try {
			do {
				// System.out.println(c.getState());
				c.waitForStaitChange();
			} while (c.getState() != ConnectionState.OFFLINE);
			// System.out.println(c.getState());
			// System.out.println("ConnectionStateWatcher d�d");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}