package se.fivefactorial.network.connection;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

public class TokenList {

	private static final int STEP = 10;

	private HashSet<Token> set;
	private ArrayList<Token> list;

	public TokenList() {
		set = new HashSet<>();
		list = new ArrayList<>();
	}

	public synchronized Token get() {
		if (list.isEmpty())
			fill();
		int i = new Random().nextInt(list.size());
		return list.remove(i);
	}

	private synchronized void fill() {
		int max = set.size();
		for (int i = max; i < max + STEP; i++) {
			Token t = new Token(i);
			set.add(t);
			list.add(t);
		}
	}

	public synchronized void give(Token t) {
		list.add(t);
	}

	

}
