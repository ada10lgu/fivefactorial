package se.fivefactorial.network.connection;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import se.fivefactorial.network.verbose.Verbose;

public class Server extends Thread {

	private ServerSocket ss;
	private ConnectionHandler ch;
	private Verbose verbose;

	public Server(int port, ConnectionHandler ch, Verbose verbose) throws IOException {
		this.ss = new ServerSocket(port);
		this.ch = ch;
		this.verbose = verbose;
	}

	@Override
	public void run() {
		verbose.print("Server started");
		while (!isInterrupted()) {
			try {
				Socket s = ss.accept();
				ch.addConnection(s);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public synchronized void interrupt() {
		super.interrupt();
		try {
			ss.close();
		} catch (IOException e) {
			System.err.println("Server: Could not close socket");
			e.printStackTrace();
		}
	}

	public void close() throws IOException {
		ss.close();
	}

}
