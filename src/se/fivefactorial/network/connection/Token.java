package se.fivefactorial.network.connection;

import java.nio.ByteBuffer;

public class Token {

	private int i;

	Token(int i) {
		this.i = i;
	}

	public Token(byte[] b) {
		ByteBuffer bb = ByteBuffer.wrap(b);
		i = bb.getInt();
	}

	public byte[] toByte() {
		return ByteBuffer.allocate(4).putInt(i).array();
	}

	@Override
	public int hashCode() {
		return i;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Token)
			return obj.hashCode() == hashCode();
		return false;
	}

	@Override
	public String toString() {
		return "Token #" + i;
	}

}
