package se.fivefactorial.network.connection;

import java.net.Socket;

public interface ConnectionHandler {
	public void addConnection(Socket s);
}
